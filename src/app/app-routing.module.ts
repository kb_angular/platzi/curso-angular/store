import { NgModule, Component } from '@angular/core';
// PreloadAllModules -> para que cargue sin problemas en redes lentas como 3g
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';
import { ProductsComponent } from './products/products.component';
import { ContactComponent } from './contact/contact.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { ProductDetailComponent } from './product-detail/product-detail.component';
import { LayoutComponent } from './layout/layout.component';
import { AdminGuard } from './admin.guard';

const routes: Routes = [
  { path: '', component: LayoutComponent, children: [
    { path: '', pathMatch: 'full', redirectTo: 'home' },
    { path: 'home', loadChildren: () => import('./home/home.module').then(m => m.HomeModule ) },
    { path: 'products', component: ProductsComponent },
    { path: 'contact', component: ContactComponent, canActivate: [AdminGuard] },
    { path: 'products/:id', component: ProductDetailComponent }
  ]},
  // **  -> significa si no hace match con ninguna ruta
  { path: '**', component: PageNotFoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {preloadingStrategy: PreloadAllModules})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
