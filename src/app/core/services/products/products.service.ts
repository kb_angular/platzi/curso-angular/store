import { Injectable } from '@angular/core';
import { Product } from '../../../product.model';


@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  constructor() { }

  getAllProducts() {
    return this.products;
  }

  getProduct(id: string){
    return this.products.find(item => id === item.id);
  }

  products: Product[] = [
    {
      id: '1',
      image: 'assets/images/camiseta.png',
      title: 'Camiseta',
      price: 90000,
      description: 'bla bla bla'
    },
    {
      id: '2',
      image: 'assets/images/hoodle.png',
      title: 'Hoodle',
      price: 30000,
      description: 'bla bla bla'
    },
    {
      id: '3',
      image: 'assets/images/mug.png',
      title: 'Mug',
      price: 50000,
      description: 'bla bla bla'
    },
    {
      id: '4',
      image: 'assets/images/pin.png',
      title: 'Pin',
      price: 10000,
      description: 'bla bla bla'
    },
    {
      id: '5',
      image: 'assets/images/stickers1.png',
      title: 'Stickers1',
      price: 90000,
      description: 'bla bla bla'
    },
    {
      id: '6',
      image: 'assets/images/stickers2.png',
      title: 'Stickers2',
      price: 60000,
      description: 'bla bla bla'
    }
  ];
}
