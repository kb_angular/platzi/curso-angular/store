# Store

# Comandos
    ng lint                     --> que no tenga una error
    ng g c caperta/componente   --> Crear un componente  con una carpeta espcifica
    ng g s caperta/servicio     --> Crear un servicio en una carpeta especifica
    ng g p nombre               --> Crear un pipes
    ng g d nombre               --> Crear una directiva, no permite hacer cambios en el DOM del HTML dinamicamente.
    ng g m nombre               --> Crear un modulo
    ng g g nombre               --> Crea un guardian, para proteger



# Los Principios Solid          
    Con el uso de los Input y Output ayuda a los principios SOLID (de una sola responsabilidad).

    input                         -> Recibe informacion de otro lugar. Este otro lugar se envia a traves de []
    output con EventEmitter       -> Envia informacion y lo recepciona con ()


## Ciclo de vida de componentes:
    1. Constructor  -> Para crear nuesro componente y ponerlo en inteterfaz
    2. OnChanges    -> Detectar cuando hay un cambio de cada INPUT. 
    3. OnInit       -> Solo se ejecuta una vez, detecta cuando el componente ya esta lista en interfaz.
    4. DoCheck      -> Solo una vez, detecta cuando los componentes hijos ya esta lista en interfaz
        4.1.    AfterContentChecked -> 
        4.1.    AfterContentChecked ->
        4.2.    AfterViewInit       ->
        4.3.    AfterViewCheked     ->
    5. OnDestroy    -> Detecta cuando el elementos es quitado desde la interfaz.

## Pipe
    Transformacion de la data sin alterarla. 

    | uppercase --> transforma a mayusca
    | slice     --> cortar desde un inicio hacia un final

## Modulos
    Hay dos 2 modulos especiales
    * core      --> guarda todos todos los servicios que vamos a utilizar en otros modulo. Genera UNA sola referencia. ejemplo> serv autenticacoin
    * shared    --> guarda componentes y servicios compartidos. ejemplo> componentes que es utilizado por varios modulos.

## Libreria para Banner
    https://idangero.us/  ( https://swiperjs.com/ )      --> Libreria para banner



## Template para pagina 404
    https://freefrontend.com/html-css-404-page-templates/

    



