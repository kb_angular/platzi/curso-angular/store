import { Component, OnInit } from '@angular/core';
import { Product } from '../product.model';
import { ProductsService } from '../core/services/products/products.service';



@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {

  listProducts: Product[];

  constructor(private products: ProductsService) { }

  ngOnInit(): void {
    this.listProducts = this.products.getAllProducts();
  }

  clickProduct(id: number){
    console.log('Product');
    console.log(id);
  }

}
