import { NgModule } from '@angular/core';

// para las directivas como ngIf, ngSwtich, ngFor, cuando estan fuera del modulo principal
import { CommonModule } from '@angular/common';


import { BannerComponent } from './components/banner/banner.component';
import { HomeComponent } from './components/home/home.component';

import { HomeRoutingModule } from './home-routing.module';
import { SharedModule } from '../shared/shared.module';

@NgModule({
    declarations: [
        BannerComponent,
        HomeComponent
    ],
    imports: [
        CommonModule,
        HomeRoutingModule,
        // Importamos SharedModule porque se requiere
        SharedModule
    ],
    providers: []
    })

export class HomeModule {}