import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Product } from '../product.model';

//Decorador con un selector y un template
@Component({
    selector: 'app-product',
    templateUrl: './product.component.html',
    styleUrls: ['./product.component.css']
})

//Clase
export class ProductComponent{

    @Input() product: Product;
    @Output() productClicked: EventEmitter<any> = new EventEmitter();

    addCart() {
        console.log('anadir al carrito');
        this.productClicked.emit(this.product.id);
    }

    today = new Date();
}