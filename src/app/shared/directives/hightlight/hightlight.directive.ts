import { Directive, ElementRef } from '@angular/core';
import { element } from 'protractor';

@Directive({
  selector: '[appHightlight]'
})
export class HightlightDirective {
  // inyeccion de dependencia
  constructor(element: ElementRef) {
    element.nativeElement.style.background = 'red';
   }

}
