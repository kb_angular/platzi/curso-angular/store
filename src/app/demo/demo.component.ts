import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-demo',
  templateUrl: './demo.component.html',
  styleUrls: ['./demo.component.css']
})
export class DemoComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  title = 'Demo';

  items = ['nicolas', 'julian', 'perez'];

  addItem() {
    this.items.push('nuevo Item');
  }

  deleteItem(index: number) {
    this.items.splice(index, 1);
  }

  power = 10;

}
