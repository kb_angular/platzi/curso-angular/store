import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ExponentialPipe } from './pipes/exponential/exponential.pipe';
import { HightlightDirective } from './directives/hightlight/hightlight.directive';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { RouterModule } from '@angular/router';



@NgModule({
  declarations: [
    ExponentialPipe,
    HightlightDirective,
    HeaderComponent,
    FooterComponent
  ],
  exports: [
    // Agreamos los componentes, que van a ser utilizado en otros componentes
    ExponentialPipe,
    HightlightDirective,
    HeaderComponent,
    FooterComponent
  ],
  imports: [
    CommonModule,
    RouterModule
  ]
})
export class SharedModule { }
